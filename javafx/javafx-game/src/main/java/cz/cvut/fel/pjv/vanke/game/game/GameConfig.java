package cz.cvut.fel.pjv.vanke.game.game;

import cz.cvut.fel.pjv.vanke.game.game.npc.Character;

import java.util.List;

public interface GameConfig {
    List<Character> getCharacters();
}
