package cz.cvut.fel.pjv.vanke.game.game;

import cz.cvut.fel.pjv.vanke.game.game.npc.Character;
import cz.cvut.fel.pjv.vanke.game.game.npc.NpcCharacter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class TestGameConfig implements GameConfig{

    private static final int BOUNDS = 200;

    @Override
    public List<Character> getCharacters() {
        List<Character> characters = new ArrayList<>();
        ThreadLocalRandom random = ThreadLocalRandom.current();
        for (int i = 0; i < 5; i++) {
            characters.add(getRandomCharacter(random.nextDouble(BOUNDS), random.nextDouble(BOUNDS)));
        }
        return characters;
    }

    private NpcCharacter getRandomCharacter(double x, double y) {
        return new NpcCharacter(x, y);
    }
}
