package cz.cvut.fel.pjv.vanke.game;

import cz.cvut.fel.pjv.vanke.game.controller.GameController;
import cz.cvut.fel.pjv.vanke.game.game.Game;
import cz.cvut.fel.pjv.vanke.game.game.GameConfig;
import cz.cvut.fel.pjv.vanke.game.game.GameLoop;
import cz.cvut.fel.pjv.vanke.game.game.TestGameConfig;
import cz.cvut.fel.pjv.vanke.game.view.GameView;
import cz.cvut.fel.pjv.vanke.game.view.View;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        View view = new GameView();
        // config
        GameConfig gameConfig = new TestGameConfig();
        Game game = new Game();
        game.initialize(gameConfig);
        // loop
        GameLoop gameLoop = new GameLoop(game);
        Scene scene = view.getScene();

        // controller
        GameController controller = view.getController();
        controller.initializeGameLoop(gameLoop);
        controller.initializeGame(game);

        stage.addEventHandler(KeyEvent.KEY_PRESSED, controller.getKeyHandler());
        stage.setScene(scene);
        stage.show();
    }
}
