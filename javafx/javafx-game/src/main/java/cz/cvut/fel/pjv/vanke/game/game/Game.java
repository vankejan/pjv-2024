package cz.cvut.fel.pjv.vanke.game.game;

import cz.cvut.fel.pjv.vanke.game.game.npc.Character;
import javafx.scene.canvas.GraphicsContext;

import java.util.List;

public class Game {

    private List<Character> characters;
    private GraphicsContext graphicsContext;

    public void initialize(GameConfig gameConfig) {
        characters = gameConfig.getCharacters();
    }

    public void setGraphicsContext(GraphicsContext graphicsContext) {
        this.graphicsContext = graphicsContext;
    }

    public void update() {
        for (Character character : characters) {
            character.move();
        }
    }

    public void render() {
        graphicsContext.clearRect(0, 0, 500, 500);
        for (Character character : characters) {
            character.draw(graphicsContext);
        }
    }
}
