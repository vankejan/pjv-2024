package cz.cvut.fel.pjv.vanke.game.view;

import cz.cvut.fel.pjv.vanke.game.controller.GameController;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;

import java.io.IOException;

public class GameView implements View {

    private GameController controller;

    @Override
    public Scene getScene() {
        FXMLLoader fxmlLoader = new FXMLLoader(GameView.class.getResource("/fxml/primary.fxml"));

        Parent root;
        try {
            root = fxmlLoader.load();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        controller = fxmlLoader.getController();

        return new Scene(root);
    }

    @Override
    public GameController getController() {
        return this.controller;
    }

    @Override
    public EventHandler<KeyEvent> getKeyHandler() {
        return this.controller.getKeyHandler();
    }
}
