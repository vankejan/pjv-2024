package cz.cvut.fel.pjv.vanke.game.controller;

import cz.cvut.fel.pjv.vanke.game.game.Game;
import cz.cvut.fel.pjv.vanke.game.game.GameLoop;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GameController implements Initializable {

    @FXML
    private Label amountCounter;

    @FXML
    private Canvas gameCanvas;

    private GameLoop gameLoop;

    private SimpleIntegerProperty amount = new SimpleIntegerProperty(0);

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gameCanvas.getGraphicsContext2D()
                .fillText("Click to start", 100, 100, 100);

        gameCanvas.onMouseClickedProperty().setValue(mouseEvent -> {
            amount.setValue(amount.getValue() + 1);
        });

        amountCounter.setFont(new Font(200));
        amountCounter.textProperty().bind(amount.asString());
    }

    public void initializeGameLoop(GameLoop gameLoop) {
        this.gameLoop = gameLoop;
    }

    public void initializeGame(Game game) {
        game.setGraphicsContext(gameCanvas.getGraphicsContext2D());

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(() -> {
           while (true) {
               Thread.sleep(1000);
               Platform.runLater(() -> {
                   amount.setValue(amount.getValue() + 10);
               });
           }
        });
    }

    @FXML
    private void startGame(MouseEvent mouseEvent) {
        this.gameLoop.start();
    }

    public EventHandler<KeyEvent> getKeyHandler() {
        return keyEvent -> {
            switch (keyEvent.getCode()) {
                case SPACE -> this.gameLoop.pause();
                case ENTER -> System.out.println(amount.getValue());
            }
        };
    }
}
