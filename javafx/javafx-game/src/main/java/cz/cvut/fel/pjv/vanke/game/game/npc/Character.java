package cz.cvut.fel.pjv.vanke.game.game.npc;

import javafx.scene.canvas.GraphicsContext;

public interface Character {

    void move();

    void draw(GraphicsContext graphicsContext);
}
