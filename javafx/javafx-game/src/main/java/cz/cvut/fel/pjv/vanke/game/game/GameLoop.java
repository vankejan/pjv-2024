package cz.cvut.fel.pjv.vanke.game.game;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GameLoop {

    private boolean paused;
    public Game game;

    public GameLoop(Game game) {
        this.game = game;
    }

    public void start() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(() -> {
            while (!paused) {
                System.out.println("Started");
                try {
                    Thread.sleep(30);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                game.update();
                game.render();
            }
        });
    }

    public void pause() {
        if (paused) {
            start();
        }
        this.paused = !paused;
    }
}
