package cz.cvut.fel.pjv.vanke.game.view;

import cz.cvut.fel.pjv.vanke.game.controller.GameController;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;

public interface View {

    Scene getScene();

    GameController getController();

    EventHandler<KeyEvent> getKeyHandler();
}
