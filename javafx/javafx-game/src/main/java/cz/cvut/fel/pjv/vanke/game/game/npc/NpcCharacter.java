package cz.cvut.fel.pjv.vanke.game.game.npc;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class NpcCharacter implements Character{

    private double x = 100;
    private double y = 100;

    public NpcCharacter() {
    }

    public NpcCharacter(double x, double y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void move() {
        if (this.x >= 0) {
            this.x -= 1;
        }
    }

    @Override
    public void draw(GraphicsContext graphicsContext) {
        graphicsContext.setFill(Color.AQUAMARINE);
        graphicsContext.fillRect(this.x, this.y, 10, 10);
    }
}
