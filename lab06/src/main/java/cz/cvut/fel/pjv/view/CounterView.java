package cz.cvut.fel.pjv.view;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

public class CounterView implements View {

    private final Label labelCounter = new Label("0");
    private final Button buttonIncrement = new Button();
    private final Button buttonReset = new Button();
    private Scene scene;

    private BorderPane createRoot() {
        BorderPane borderPane = new BorderPane();
        buttonIncrement.setText("Increment");
        buttonReset.setText("Reset");
        HBox hBox = new HBox();
        hBox.getChildren().addAll(buttonIncrement, buttonReset);
        borderPane.setCenter(labelCounter);
        borderPane.setBottom(hBox);
        return borderPane;
    }

    @Override
    public Scene createScene() {
        scene = new Scene(createRoot());
        return scene;
    }

    public Label getLabelCounter() {
        return labelCounter;
    }

    public Button getButtonIncrement() {
        return buttonIncrement;
    }

    public Button getButtonReset() {
        return buttonReset;
    }

    public Scene getScene() {
        return scene;
    }
}
