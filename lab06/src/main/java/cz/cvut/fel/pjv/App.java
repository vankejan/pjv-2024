package cz.cvut.fel.pjv;

import javafx.application.Application;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Application entrypoint
 */
public class App extends Application {


    private Button buttonToMenu = new Button();
    private Scene primaryScene;

    private Scene generateSimpleScene() {
        BorderPane borderPane = new BorderPane();

        Label label = new Label("Hello world");
        label.setFont(new Font(40));
        label.setTextFill(Color.rgb(255, 0, 0));

        borderPane.setTop(label);

        Button firstMenuButton = new Button("First");
        Button secondMenuButton = new Button("Second");

        VBox leftMenu = new VBox();
        leftMenu.setSpacing(10);
        leftMenu.setAlignment(Pos.BOTTOM_CENTER);
        buttonToMenu.setText("To menu");
        leftMenu.getChildren().addAll(firstMenuButton, secondMenuButton, buttonToMenu);
        borderPane.setLeft(leftMenu);


        Text text = new Text("Delší text...");
        borderPane.setCenter(text);

        Button disableAll = new Button("Click me!");
        borderPane.setBottom(disableAll);

        return new Scene(borderPane);
    }

    private Scene generateFxmlScene() throws IOException {
        // load Root component/container
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/primary.fxml"));
        Parent root = fxmlLoader.load();

        // create new scene with root
        return new Scene(root);
    }

    @Override
    public void start(Stage stage) throws IOException {
        primaryScene = generateSimpleScene();

        // set scene to stage
        stage.setScene(primaryScene);
        stage.show();

        buttonToMenu.setOnAction(actionEvent -> {
            try {
                stage.setScene(generateFxmlScene());
            } catch (IOException e) {
                System.out.println("Error");
            }
            stage.show();
        });

        buttonToPrimary.setOnAction(actionEvent -> {
            stage.setScene(primaryScene);
            stage.show();
        });
    }

    public static void main(String[] args) {
        launch();
    }

}