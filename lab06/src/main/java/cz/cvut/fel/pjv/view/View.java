package cz.cvut.fel.pjv.view;

import javafx.scene.Scene;

public interface View {

    Scene createScene();
}
