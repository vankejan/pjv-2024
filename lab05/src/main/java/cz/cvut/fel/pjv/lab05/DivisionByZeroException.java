package cz.cvut.fel.pjv.lab05;

public class DivisionByZeroException extends Exception {

    public DivisionByZeroException(String message) {
        super(message);
    }
}
