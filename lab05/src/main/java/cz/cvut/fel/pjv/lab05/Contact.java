package cz.cvut.fel.pjv.lab05;

import java.util.Objects;

public class Contact implements Comparable<Contact> {

    private final String name;
    private final String surname;

    public Contact(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Contact contact = (Contact) o;
//        return Objects.equals(getName(), contact.getName()) && Objects.equals(getSurname(), contact.getSurname());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getName(), getSurname());
//    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }

    @Override
    public int compareTo(Contact o) {
        if (this.getName().equals(o.getName())) {
            return this.getSurname().compareTo(o.getSurname());
        }
        return this.getName().compareTo(o.getName());
    }
}
