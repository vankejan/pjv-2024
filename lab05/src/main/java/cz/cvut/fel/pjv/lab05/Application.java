package cz.cvut.fel.pjv.lab05;

import java.util.*;

public class Application {

    public static void main(String[] args) {
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Karel", "Novák"));
        contacts.add(new Contact("Adam", "Vojtěch"));
        contacts.add(new Contact("Miloš", "Zeman"));
        contacts.add(new Contact("Adam", "Dvořák"));

        Comparator<Contact> contactComparator = Comparator.comparing(Contact::getName);
        contactComparator = contactComparator.thenComparing(Contact::getSurname);
        contactComparator = contactComparator.reversed();
        contacts.sort(contactComparator);

        // System.out.println(contacts);

        Set<Contact> contactSet = new HashSet<>();
        Contact contact = new Contact("Karel", "Novák");
        contactSet.add(contact);
        contactSet.add(new Contact("Adam", "Vojtěch"));
        contactSet.add(new Contact("Miloš", "Zeman"));
        contactSet.add(new Contact("Adam", "Dvořák"));

        // System.out.println(contactSet);

        Map<String, List<Contact>> contactMap = new LinkedHashMap<>();
        contactMap.put("Praha", List.of(contact, new Contact("Adam", "Procházka")));
        contactMap.put("Aš", List.of(new Contact("Adam", "Vojtěch"), contact));
        contactMap.put("Plzeň", List.of(new Contact("Adam", "Dvořák")));

//        for (String key: contactMap.keySet()) {
//            System.out.println(key);
//            for (Contact entry : contactMap.get(key)) {
//                System.out.print(entry);
//            }
//            System.out.println();
//        }

        String firstString = "12";
        int second = 12;

        try {
            int first = Integer.parseInt(firstString);
            divideNumbers(first, second);
        } catch (DivisionByZeroException e) {
            System.out.println("Chyba!");
        } finally {
            System.out.println("Finally!");
        }
    }

    private static int divideNumbers(int first, int second) throws DivisionByZeroException {
        if (second == 0) {
            throw new DivisionByZeroException("Division by zero!!!");
        }
        return first / second;
    }
}
