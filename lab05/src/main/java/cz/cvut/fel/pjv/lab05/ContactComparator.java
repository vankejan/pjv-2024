package cz.cvut.fel.pjv.lab05;

import java.util.Comparator;

public class ContactComparator implements Comparator<Contact> {
    @Override
    public int compare(Contact o1, Contact o2) {
        if (o1.getName().equals(o2.getName())) {
            return o1.getSurname().compareTo(o2.getSurname());
        }
        return o1.getName().compareTo(o2.getName());
    }
}
