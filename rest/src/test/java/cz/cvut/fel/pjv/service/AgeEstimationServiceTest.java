package cz.cvut.fel.pjv.service;

import cz.cvut.fel.pjv.model.AgeEstimation;
import cz.cvut.fel.pjv.model.Person;
import cz.cvut.fel.pjv.rest.AgeEstimator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
public class AgeEstimationServiceTest {

    @Mock
    private AgeEstimator ageEstimator;

    @InjectMocks
    private AgeEstimationService ageEstimationService;

    @BeforeEach
    void setUp() {
        ageEstimationService = new AgeEstimationService(ageEstimator);
    }

    @Test
    public void testAgeEstimationService() throws URISyntaxException, IOException, InterruptedException {
        Mockito.doReturn(new AgeEstimation(25, 252555, "Jan"))
                .when(ageEstimator).getEstimatedAge(anyString());

        //Mockito.when(ageEstimator.getEstimatedAge(any())).thenReturn(new AgeEstimation(25, 252555, "Jan"));

        Person person = new Person("Jan", "Novák");

        int age = ageEstimationService.estimateAge(person);


        Assertions.assertThat(age)
                .isEqualTo(25);

    }
}