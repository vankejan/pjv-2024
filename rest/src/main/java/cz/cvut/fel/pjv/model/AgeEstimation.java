package cz.cvut.fel.pjv.model;

public class AgeEstimation {

    private final int age;
    private final int count;
    private final String name;

    public AgeEstimation(int age, int count, String name) {
        this.age = age;
        this.count = count;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public int getCount() {
        return count;
    }

    public String getName() {
        return name;
    }
}
