package cz.cvut.fel.pjv.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.pjv.model.AgeEstimation;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class AgeEstimator {

    private final ObjectMapper objectMapper = new ObjectMapper();

    //   public int getEstimatedAge(String name) {
//        AgeEstimation ageEstimation = HttpClient.create()
//                .request(HttpMethod.GET)
//                .uri("https://api.agify.io?name="+name)
//                .responseSingle(((httpClientResponse, byteBufMono) -> byteBufMono.asString().map(it -> {
//                    try {
//                        return objectMapper.readValue(it, AgeEstimation.class);
//                    } catch (JsonProcessingException e) {
//                        throw new RuntimeException(e);
//                    }
//                }))).block();
//        return ageEstimation.getAge();
    //  }

    public AgeEstimation getEstimatedAge(String name) throws URISyntaxException, IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("https://api.agify.io?name=" + name))
                .GET()
                .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        return objectMapper.readValue(response.body(), AgeEstimation.class);
    }
}
