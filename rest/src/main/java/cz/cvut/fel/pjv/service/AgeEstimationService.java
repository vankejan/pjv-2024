package cz.cvut.fel.pjv.service;

import cz.cvut.fel.pjv.rest.AgeEstimator;
import cz.cvut.fel.pjv.model.Person;

import java.io.IOException;
import java.net.URISyntaxException;

public class AgeEstimationService {

    private final AgeEstimator ageEstimator;

    public AgeEstimationService(AgeEstimator ageEstimator) {
        this.ageEstimator = ageEstimator;
    }

    public int estimateAge(Person person) throws URISyntaxException, IOException, InterruptedException {
        return ageEstimator.getEstimatedAge(person.getName()).getAge();
    }
}
