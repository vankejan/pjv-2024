package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.rest.AgeEstimator;

import java.io.IOException;
import java.net.URISyntaxException;

public class Main {

    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        AgeEstimator ageEstimator = new AgeEstimator();


        System.out.println(ageEstimator.getEstimatedAge("Jan"));
    }
}
