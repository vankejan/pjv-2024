package cz.cvut.fel.pjv.zidekja2.scenes;

import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


public class MouseClickTestSceneGenerator implements SceneGenerator {

    private static final int PANE_WIDTH = 1200;
    private static final int PANE_HEIGHT = 900;

    private final Image backgroundImage;

    private final List<Circle> circles;

    public MouseClickTestSceneGenerator() {
        backgroundImage = new Image(MouseClickTestSceneGenerator.class.getResourceAsStream("/background.png"));
        circles = new ArrayList<>();
    }

    @Override
    public Scene generateScene() {
        BorderPane borderPane = new BorderPane();

        Label title = new Label("My first clickable App!");
        title.setFont(new Font(32));
        title.setTextFill(Color.rgb(100, 170, 12));
        borderPane.setTop(title);

        Pane pane = new Pane();
        pane.setMaxSize(PANE_WIDTH, PANE_HEIGHT);

        pane.setBackground(new Background(
                new BackgroundImage(backgroundImage, null, null, null,
                                    new BackgroundSize(PANE_WIDTH, PANE_HEIGHT, false, false, false, false))));

        pane.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            double x = mouseEvent.getX();
            double y = mouseEvent.getY();

            Circle randCircle = generateRandomCircle(x, y);
            circles.add(randCircle);
            pane.getChildren().add(randCircle);
        });

        borderPane.setCenter(pane);
        return new Scene(borderPane);
    }

    @Override
    public EventHandler<KeyEvent> getKeyHandler() {
        return keyEvent -> {
            switch (keyEvent.getCode()) {
                case UP:
                case W:
                    for (Circle circle : circles) {
                        circle.setRadius(circle.getRadius() + 100);
                    }
                    break;
                case DOWN:
                case S:
                    for (Circle circle : circles) {
                        circle.setRadius(circle.getRadius() - 1);
                    }
                    break;
                default:
                    // unknown key
                    break;
            }
        };
    }

    private Circle generateRandomCircle(double x, double y) {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        Color color = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));
        return new Circle(x, y, random.nextInt(50, 130), color);
    }

}
