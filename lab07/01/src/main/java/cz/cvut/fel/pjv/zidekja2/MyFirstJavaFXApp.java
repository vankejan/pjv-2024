package cz.cvut.fel.pjv.zidekja2;

import cz.cvut.fel.pjv.zidekja2.scenes.CanvasSceneGenerator;
import cz.cvut.fel.pjv.zidekja2.scenes.MouseClickTestSceneGenerator;
import cz.cvut.fel.pjv.zidekja2.scenes.SceneGenerator;
import cz.cvut.fel.pjv.zidekja2.scenes.SimpleSceneGenerate;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class MyFirstJavaFXApp extends Application {

    @Override
    public void start(Stage stage) {
//        SceneGenerator sceneGenerator = new SimpleSceneGenerate();
//        SceneGenerator sceneGenerator = new MouseClickTestSceneGenerator();
        SceneGenerator sceneGenerator = new CanvasSceneGenerator();

        Scene scene = sceneGenerator.generateScene();

        stage.setTitle("My first App!");

        if (sceneGenerator.getKeyHandler() != null) {
            stage.addEventHandler(KeyEvent.KEY_PRESSED, sceneGenerator.getKeyHandler());
        }
        stage.setScene(scene);
        stage.setMaximized(true);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
