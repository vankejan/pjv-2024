package cz.cvut.fel.pjv.zidekja2.objects;

import javafx.scene.canvas.GraphicsContext;

public interface CanvasObject {

    void paintToCanvas(GraphicsContext graphicsContext);

}
