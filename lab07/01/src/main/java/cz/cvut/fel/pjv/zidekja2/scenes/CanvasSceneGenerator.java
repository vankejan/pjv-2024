package cz.cvut.fel.pjv.zidekja2.scenes;

import cz.cvut.fel.pjv.zidekja2.objects.CanvasObject;
import cz.cvut.fel.pjv.zidekja2.objects.Rectangle;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class CanvasSceneGenerator implements SceneGenerator {

    private static final int CANVAS_WIDTH = 1200;
    private static final int CANVAS_HEIGHT = 900;

    private final Image backgroundImage;

    private final List<CanvasObject> canvasObjects;
    private final List<Rectangle> rectangleList;

    private final Canvas canvas;

    public CanvasSceneGenerator() {
        backgroundImage = new Image(MouseClickTestSceneGenerator.class.getResourceAsStream("/background.png"));
        rectangleList = new ArrayList<>();
        canvasObjects = new ArrayList<>();
        canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    }

    @Override
    public Scene generateScene() {
        BorderPane borderPane = new BorderPane();

        Label title = new Label("My first clickable App!");
        title.setFont(new Font(32));
        title.setTextFill(Color.rgb(100, 170, 12));
        borderPane.setTop(title);

        repaintCanvas();

        canvas.addEventHandler(MouseEvent.MOUSE_PRESSED, mouseEvent -> {
            Rectangle rectangle = generateRandomRectangle(mouseEvent.getX(), mouseEvent.getY());
            rectangleList.add(rectangle);
            canvasObjects.add(rectangle);
            rectangle.paintToCanvas(canvas.getGraphicsContext2D());
        });

        borderPane.setCenter(canvas);

        return new Scene(borderPane);
    }

    @Override
    public EventHandler<KeyEvent> getKeyHandler() {
        return keyEvent -> {
            int moveSize = 2;

            switch (keyEvent.getCode()) {
                case UP:
                case W:
                    for (Rectangle rectangle : rectangleList) {
                        rectangle.setY(rectangle.getY() - moveSize);
                    }
                    break;
                case DOWN:
                case S:
                    for (Rectangle rectangle : rectangleList) {
                        rectangle.setY(rectangle.getY() + moveSize);
                    }
                    break;
                case A:
                case LEFT:
                    for (Rectangle rectangle : rectangleList) {
                        rectangle.setX(rectangle.getX() - moveSize);
                    }
                    break;
                case D:
                case RIGHT:
                    for (Rectangle rectangle : rectangleList) {
                        rectangle.setX(rectangle.getX() + moveSize);
                    }
                    break;
            }

            repaintCanvas();
        };
    }

    private Rectangle generateRandomRectangle(double x, double y) {
        ThreadLocalRandom random = ThreadLocalRandom.current();

        Color color = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));

        return new Rectangle(x, y, random.nextInt(150), random.nextInt(150), color);
    }

    private void repaintCanvas() {
        canvas.getGraphicsContext2D()
              .clearRect(0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);
        canvas.getGraphicsContext2D()
              .drawImage(backgroundImage, 0, 0, CANVAS_WIDTH, CANVAS_HEIGHT);

        for (CanvasObject canvasObject : canvasObjects) {
            canvasObject.paintToCanvas(canvas.getGraphicsContext2D());
        }
    }
}
