package cz.cvut.fel.pjv.zidekja2.scenes;

import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

import java.util.ArrayList;
import java.util.List;


public class SimpleSceneGenerate implements SceneGenerator {

    private final List<Button> leftMenuButtons = new ArrayList<>();

    @Override
    public Scene generateScene() {
        BorderPane borderPane = new BorderPane();

        Label label = new Label("Title");
        label.setFont(new Font(40));
        label.setTextFill(Color.rgb(255, 0, 0));
        borderPane.setTop(label);

        Text text = new Text("Muj text je: ");

        leftMenuButtons.add(this.generateButton("1", text));
        leftMenuButtons.add(this.generateButton("2", text));
        leftMenuButtons.add(this.generateButton("3", text));

        VBox leftMenu = new VBox();
        leftMenu.setSpacing(10);
        leftMenu.setAlignment(Pos.BOTTOM_CENTER);
        leftMenu.getChildren().addAll(leftMenuButtons);

        borderPane.setLeft(leftMenu);

        borderPane.setCenter(text);

        Button disableAll = new Button("Disable!");
        disableAll.setOnMouseClicked(mouseEvent -> {
            for (Button leftMenuButton : leftMenuButtons) {
                leftMenuButton.setDisable(true);
            }
        });

        borderPane.setBottom(disableAll);

        return new Scene(borderPane);
    }

    @Override
    public EventHandler<KeyEvent> getKeyHandler() {
        return null;
    }

    private Button generateButton(String text, Text textToGenerate) {
        Button button = new Button(text);
        button.setOnMouseClicked(mouseEvent -> textToGenerate.setText(textToGenerate.getText() + text));
        return button;
    }

}
