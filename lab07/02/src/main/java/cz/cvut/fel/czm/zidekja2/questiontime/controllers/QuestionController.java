package cz.cvut.fel.czm.zidekja2.questiontime.controllers;

import cz.cvut.fel.czm.zidekja2.questiontime.domain.Question;
import cz.cvut.fel.czm.zidekja2.questiontime.logic.question.QuestionProvider;
import cz.cvut.fel.czm.zidekja2.questiontime.logic.score.BasicScoreHandler;
import cz.cvut.fel.czm.zidekja2.questiontime.logic.score.ScoreHandler;
import javafx.application.HostServices;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import lombok.Setter;
import lombok.extern.java.Log;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Log
public class QuestionController {

    private static final int NUMBER_OF_QUESTIONS = 5;

    @Setter
    private HostServices hostServices;

    private QuestionProvider questionProvider;
    private Question currentQuestion;
    private ScoreHandler scoreHandler;

    @FXML
    private FlowPane buttons;

    @FXML
    private Hyperlink email;

    @FXML
    private Label questionText;

    @FXML
    private Text result;

    @FXML
    private Label statistics;

    public void initialize() {
        this.questionProvider = new QuestionProvider(NUMBER_OF_QUESTIONS);
        this.scoreHandler = new BasicScoreHandler();

        this.prepareNextQuestion();

        log.info("QuestionController initialized.");
    }

    @FXML
    private void prepareNextQuestion() {
        this.currentQuestion = this.questionProvider.getNext();

        this.buttons.getChildren()
                .clear();
        if (this.currentQuestion != null) {
            this.questionText.setText(this.currentQuestion.getId() + ", " + this.currentQuestion.getText());

            List<String> possibleAnswers = this.currentQuestion.getPossibleAnswers();
            for (int i = 0; i < possibleAnswers.size(); i++) {
                String possibleAnswer = possibleAnswers.get(i);

                Button button = new Button(possibleAnswer);
                button.setOnMouseClicked(this.answerHandler(i));

                this.buttons.getChildren()
                        .add(button);
            }
        } else {
            this.questionText.setText("Hotovo");
            this.buttons.getChildren()
                    .clear();

            this.statistics.setText(this.generateStatistics());
        }
    }

    private EventHandler<MouseEvent> answerHandler(int buttonId) {
        return mouseEvent -> {

            if (this.currentQuestion.getCorrectAnswerId() == buttonId) {
                result.setFill(Color.GREEN);
                result.setText("\u2713");

                scoreHandler.countSuccess();
            } else {
                result.setFill(Color.RED);
                result.setText("\u274C");

                scoreHandler.countFail();
            }

            Executors.newScheduledThreadPool(1)
                    .schedule(() -> result.setText(""), 2500, TimeUnit.MILLISECONDS);

            this.prepareNextQuestion();
        };
    }

    private String generateStatistics() {
        return "Správně: " + this.scoreHandler.countSuccessPercent() + "%";
    }

    public void sendMail() {
        log.info("Opening mail client.");
        hostServices.showDocument("mailto:" + email.getText());
    }
}
