package cz.cvut.fel.czm.zidekja2.questiontime.logic.question;

import cz.cvut.fel.czm.zidekja2.questiontime.domain.Question;

import java.util.*;

public class RandomQuestionsChooser implements QuestionsChooser {

    private final List<Question> allQuestions;

    public RandomQuestionsChooser(List<Question> allQuestions) {
        this.allQuestions = allQuestions;
    }

    private List<Integer> generateRandomVariation(int n, int k) {
        Random random = new Random();
        Set<Integer> chosenQuestions = new HashSet<>();

        while (chosenQuestions.size() < k) {
            int chosen = random.nextInt(n);
            chosenQuestions.add(chosen);
        }

        List<Integer> variation = new ArrayList<>(chosenQuestions);
        Collections.shuffle(variation);
        return variation;
    }

    @Override
    public List<Question> getQuestions(int numberOfQuestions) {
        List<Question> finalList;

        if (numberOfQuestions >= allQuestions.size()) {
            finalList = new ArrayList<>(allQuestions);
            Collections.shuffle(finalList);
            return finalList;
        } else {
            finalList = new ArrayList<>();
        }

        List<Integer> idx = this.generateRandomVariation(allQuestions.size(), numberOfQuestions);

        for (Integer integer : idx) {
            finalList.add(allQuestions.get(integer));
        }

        return finalList;
    }
}
