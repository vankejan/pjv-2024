package cz.cvut.fel.czm.zidekja2.questiontime.csv.converters;

import cz.cvut.fel.czm.zidekja2.questiontime.csv.QuestionsCSVQuestionRow;
import cz.cvut.fel.czm.zidekja2.questiontime.domain.Question;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class CSVConverter {

    public static List<Question> convertQuestionList(List<QuestionsCSVQuestionRow> rowList) {
        List<Question> questions = new ArrayList<>();

        for (QuestionsCSVQuestionRow row : rowList) {
            questions.add(new Question(row.getId(), row.getText(), row.getAnswers(), row.getCorrectAnswer()));
        }

        return questions;
    }

}
