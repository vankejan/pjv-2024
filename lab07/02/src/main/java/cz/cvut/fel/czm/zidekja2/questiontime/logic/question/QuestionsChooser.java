package cz.cvut.fel.czm.zidekja2.questiontime.logic.question;

import cz.cvut.fel.czm.zidekja2.questiontime.domain.Question;

import java.util.List;

public interface QuestionsChooser {

    List<Question> getQuestions(int numberOfQuestions);

}
