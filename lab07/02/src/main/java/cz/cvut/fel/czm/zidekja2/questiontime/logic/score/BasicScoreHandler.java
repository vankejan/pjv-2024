package cz.cvut.fel.czm.zidekja2.questiontime.logic.score;

import lombok.Getter;

@Getter
public class BasicScoreHandler implements ScoreHandler {

    private int totalCount = 0;

    private int totalSuccess = 0;
    private int totalFail = 0;

    @Override
    public void countFail() {
        this.totalCount++;
        this.totalFail++;
    }

    @Override
    public void countSuccess() {
        this.totalCount++;
        this.totalSuccess++;
    }

    @Override
    public double countSuccessRatio() {
        if (totalCount == 0) return 1;

        return (double) totalSuccess / totalCount;
    }

    @Override
    public double countSuccessPercent() {
        return countSuccessRatio() * 100;
    }
}
