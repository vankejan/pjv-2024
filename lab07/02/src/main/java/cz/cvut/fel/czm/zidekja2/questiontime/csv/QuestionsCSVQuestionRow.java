package cz.cvut.fel.czm.zidekja2.questiontime.csv;

import lombok.Getter;

import java.util.List;

@Getter
public class QuestionsCSVQuestionRow {

    private final int id;
    private final String text;
    private final int numberOfAnswers;
    private final int correctAnswer;
    private final List<String> answers;

    public QuestionsCSVQuestionRow(int id, String text, int numberOfAnswers, int correctAnswer, List<String> answers) {
        this.id = id;
        this.text = text;
        this.numberOfAnswers = numberOfAnswers;
        this.correctAnswer = correctAnswer;
        this.answers = answers;

        if (correctAnswer > numberOfAnswers || numberOfAnswers != answers.size()) {
            throw new IllegalArgumentException("The question of id " + id + " has invalid number of answers.");
        }
    }

    public QuestionsCSVQuestionRow(List<String> values) {
        this(Integer.parseInt(values.get(0)), values.get(1), Integer.parseInt(values.get(2)), Integer.parseInt(values.get(3)), values.subList(4, values.size()));
    }
}
