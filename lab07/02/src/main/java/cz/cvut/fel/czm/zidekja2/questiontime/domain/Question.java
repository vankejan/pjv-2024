package cz.cvut.fel.czm.zidekja2.questiontime.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Question {

    private final int id;

    private final String text;

    private final List<String> possibleAnswers;

    private final int correctAnswerId;

}
