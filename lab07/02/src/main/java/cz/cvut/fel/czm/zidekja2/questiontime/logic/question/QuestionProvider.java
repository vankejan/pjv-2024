package cz.cvut.fel.czm.zidekja2.questiontime.logic.question;

import cz.cvut.fel.czm.zidekja2.questiontime.csv.QuestionsCSVParser;
import cz.cvut.fel.czm.zidekja2.questiontime.csv.converters.CSVConverter;
import cz.cvut.fel.czm.zidekja2.questiontime.domain.Question;
import cz.cvut.fel.czm.zidekja2.questiontime.logic.exception.UnableToLoadQuestionsException;
import lombok.extern.java.Log;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

@Log
public class QuestionProvider {

    private final int numberOfQuestions;

    private List<Question> questions = null;
    private Iterator<Question> questionIterator;
    private int current = -1;

    public QuestionProvider(int numberOfQuestions) {
        this.numberOfQuestions = numberOfQuestions;
    }

    private void load() throws UnableToLoadQuestionsException {
        try {
            QuestionsCSVParser parser = new QuestionsCSVParser(getClass().getResource("/questions/questions.csv")
                                                                         .toURI());
            parser.parse();

            List<Question> allQuestions = CSVConverter.convertQuestionList(parser.getQuestionRows());

            QuestionsChooser questionsChooser = new RandomQuestionsChooser(allQuestions);
            this.questions = questionsChooser.getQuestions(this.numberOfQuestions);
        } catch (IOException | URISyntaxException e) {
            throw new UnableToLoadQuestionsException("Questions failed to load!", e);
        }
    }

    public Question getNext() {
        try {
            if (questions == null) {
                this.load();
                this.questionIterator = this.questions.iterator();
            }
        } catch (UnableToLoadQuestionsException e) {
            log.log(Level.SEVERE, e.getMessage(), e);
            return null;
        }

        if (!this.questionIterator.hasNext()) {
            return null;
        }

        return this.questionIterator.next();
    }
}
