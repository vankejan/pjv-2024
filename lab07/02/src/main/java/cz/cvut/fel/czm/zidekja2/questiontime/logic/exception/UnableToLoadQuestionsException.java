package cz.cvut.fel.czm.zidekja2.questiontime.logic.exception;

public class UnableToLoadQuestionsException extends Exception {
    public UnableToLoadQuestionsException(String message, Throwable cause) {
        super(message, cause);
    }
}
