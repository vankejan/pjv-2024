package cz.cvut.fel.czm.zidekja2.questiontime.csv;

import lombok.Getter;

import java.util.List;

@Getter
public class QuestionsCSVHeaderRow {

    private List<String> values;

    public QuestionsCSVHeaderRow(List<String> values) {
        this.values = values;
    }
}
