package cz.cvut.fel.czm.zidekja2.questiontime;

import cz.cvut.fel.czm.zidekja2.questiontime.controllers.QuestionController;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class QuestionTimeJavaFXMain extends Application {

    private Scene generateScene() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/main.fxml"));

        Parent root = fxmlLoader.load();

        QuestionController questionController = fxmlLoader.getController();
        questionController.setHostServices(getHostServices());

        return new Scene(root);
    }

    public void start(Stage stage) throws Exception {
        stage.setTitle("Question Time ??");
        stage.setResizable(false);
        stage.setScene(this.generateScene());
        stage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
