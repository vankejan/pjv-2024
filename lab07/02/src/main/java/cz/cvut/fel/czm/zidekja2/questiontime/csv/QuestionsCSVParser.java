package cz.cvut.fel.czm.zidekja2.questiontime.csv;

import lombok.Getter;
import lombok.extern.java.Log;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Log
public class QuestionsCSVParser {
    private final URI inputFile;

    @Getter
    private QuestionsCSVHeaderRow headerRow;

    @Getter
    private List<QuestionsCSVQuestionRow> questionRows;

    public QuestionsCSVParser(URI inputFile) {
        this.inputFile = inputFile;
    }

    private List<String> parseLine(String line) {
        String[] split = line.split(",");

        List<String> output = new ArrayList<>();

        boolean incompleteString = false;
        StringBuilder stringBuilder = null;
        for (String s : split) {
            if ('"' == s.charAt(0) && '"' == s.charAt(s.length() - 1)) {
                // complete string, remove quotes
                output.add(s.substring(1, s.length() - 1));
            } else if ('"' == s.charAt(0)) {
                // string has comma inside and it was mistakenly split, we need to reconstruct the string
                incompleteString = true;
                stringBuilder = new StringBuilder(s);
            } else if ('"' == s.charAt(s.length() - 1)) {
                // last part of split string
                if (stringBuilder == null) {
                    throw new IllegalArgumentException("Quotes in CSV file are not correctly opened/closed.");
                }

                String completeString = stringBuilder.toString();
                stringBuilder = null;
                incompleteString = false;

                output.add(completeString.substring(1, completeString.length() - 1));
            } else if (incompleteString) {
                // we are in process of string reconstruction
                stringBuilder.append(s);
            } else {
                // numeric value
                output.add(s);
            }
        }

        return output;
    }

    public void parse() throws IOException {
        List<String> lines = Files.readAllLines(Path.of(inputFile));

        String headerLine = lines.get(0);
        this.headerRow = new QuestionsCSVHeaderRow(parseLine(headerLine));

        this.questionRows = new ArrayList<>();
        for (int i = 1; i < lines.size(); i++) {
            this.questionRows.add(new QuestionsCSVQuestionRow(parseLine(lines.get(i))));
        }
    }
}
