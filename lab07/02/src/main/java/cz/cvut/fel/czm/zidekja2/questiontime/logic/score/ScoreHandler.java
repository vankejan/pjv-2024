package cz.cvut.fel.czm.zidekja2.questiontime.logic.score;

public interface ScoreHandler {

    void countFail();

    void countSuccess();

    double countSuccessRatio();

    double countSuccessPercent();
}
