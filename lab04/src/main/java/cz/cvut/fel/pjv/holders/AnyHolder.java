package cz.cvut.fel.pjv.holders;

import cz.cvut.fel.pjv.model.Bicycle;

public class AnyHolder<T extends Bicycle> {
    private final T item;

    public AnyHolder(T item) {
        this.item = item;
    }

    public T getItem() {
        return item;
    }
    
    public void printBikeDescription() {
        item.printDescription();
    }
}
