package cz.cvut.fel.pjv.services;

import cz.cvut.fel.pjv.model.Bicycle;
import cz.cvut.fel.pjv.model.MountainBike;
import cz.cvut.fel.pjv.model.RoadBike;

public class BasicService {

    public BasicService() {
    }

    public void accept(Bicycle bike) {
        System.out.println("fixing Bicycle.");
    }

    public void accept(MountainBike bike) {
        System.out.println("can't fix" + bike.getClass().getName());
    }

    public void accept(RoadBike bike) {
        System.out.println("can't fix" + bike.getClass().getName());
    }
}
