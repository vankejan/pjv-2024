package cz.cvut.fel.pjv.model;


import cz.cvut.fel.pjv.services.BasicService;

public class MountainBike extends Bicycle {

    private final String suspension;

    public MountainBike(int startCadence, int startSpeed, int startGear, String suspension) {
        super(startCadence, startSpeed, startGear);
        this.suspension = suspension;
    }

    @Override
    public void printDescription() {
        super.printDescription();
        System.out.println("The MountainBike has " + suspension + " suspension.");
    }

    @Override
    public void visit(BasicService basicService) {
        basicService.accept(this);
    }

}
