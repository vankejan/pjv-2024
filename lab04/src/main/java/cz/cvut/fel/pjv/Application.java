package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.enums.BikeColor;
import cz.cvut.fel.pjv.holders.BicycleHolder;
import cz.cvut.fel.pjv.holders.MountainBikeHolder;
import cz.cvut.fel.pjv.holders.RoadBikeHolder;
import cz.cvut.fel.pjv.model.Bicycle;
import cz.cvut.fel.pjv.model.MountainBike;
import cz.cvut.fel.pjv.model.RoadBike;
import cz.cvut.fel.pjv.services.BasicService;
import cz.cvut.fel.pjv.services.HoldingCar;
import cz.cvut.fel.pjv.services.MountainBikeService;
import cz.cvut.fel.pjv.services.RoadBikeService;

public class Application {

    /**
     * This method tests classes implemented in Task 1.
     */
    public static void testBikesDescription() {
        Bicycle[] bicycles = new Bicycle[3];
        bicycles[0] = new Bicycle(20, 10, 1);
        bicycles[1] = new MountainBike(20, 10, 5, "Dual");
        bicycles[2] = new RoadBike(40, 20, 8, 23);

        for (Bicycle bike : bicycles) {
            bike.printDescription();
        }
        /*
        Console output:
        Bike is in gear 1 with a cadence of 20 and travelling at a speed of 10.

        Bike is in gear 5 with a cadence of 20 and travelling at a speed of 10.
        The MountainBike has Dual suspension.

        Bike is in gear 8 with a cadence of 40 and travelling at a speed of 20.
        The RoadBike has 23mm tires.
        */
    }

    /**
     * This method tests classes naively implemented in Task 2.
     */
    public static void testBikeServices() {

        Bicycle[] bicycles = new Bicycle[3];
        bicycles[0] = new Bicycle(20, 10, 1);
        bicycles[1] = new MountainBike(20, 10, 5, "Dual");
        bicycles[2] = new RoadBike(40, 20, 8, 23);

        BasicService basicService = new BasicService();
        MountainBikeService mountainBikeService = new MountainBikeService();
        RoadBikeService roadBikeService = new RoadBikeService();

        /*
            Here compiler cannot safely decide which accept(..) method to call
            because we have 2 different child classes MountainBike and RoadBike
            and overloaded methods are bound at compilation time when the type of the bike is not known!
        */
        for (Bicycle bike : bicycles) {
            basicService.accept(bike);
        }

        // It's the same for the other two services
        mountainBikeService.accept(bicycles[1]); // put in the MountainBike
        roadBikeService.accept(bicycles[2]); // put in the RoadBike

        // One possible, but ugly and not recommended solution would be the following

        for (Bicycle bike : bicycles) {
            if (bike instanceof MountainBike) { // check what type the bike is
                System.out.println("Putting MountainBike into basicService");
                basicService.accept((MountainBike) bike); // explicitly cast

            } else if (bike instanceof RoadBike) {
                System.out.println("Putting RoadBike into basicService");
                basicService.accept((RoadBike) bike);
            } else {
                System.out.println("Putting Bike into basicService");
                basicService.accept(bike);
            }
        }

        // SEE TASK 3 for recommended solution!
    }

    /**
     * This method tests the double dispatch fix described in Task 3
     * <p>
     * Also called Visitor pattern!
     * <p>
     * Remember that in Task 1 the JVM could decide (safely) the type of the instance
     * and call the right printDescription() method.
     * The visitor pattern uses just that. Let us see.
     */
    public static void testDoubleDispatch() {
        Bicycle[] bicycles = new Bicycle[3];
        bicycles[0] = new Bicycle(20, 10, 1);
        bicycles[1] = new MountainBike(20, 10, 5, "Dual");
        bicycles[2] = new RoadBike(40, 20, 8, 23);

        BasicService basicService = new BasicService();
        MountainBikeService mountainBikeService = new MountainBikeService();
        RoadBikeService roadBikeService = new RoadBikeService();

        System.out.println("Visiting Basic Service:");
        for (Bicycle bike : bicycles) {
            bike.visit(basicService);
        }

        System.out.println("Visiting Mountain Bike Service:");
        for (Bicycle bike : bicycles) {
            bike.visit(mountainBikeService);
        }

        System.out.println("Visiting Road Bike Service:");
        for (Bicycle bike : bicycles) {
            bike.visit(roadBikeService);
        }
    }

    /**
     * Tests the holder classes described in Task 4
     */
    public static void testHolders() {

        Bicycle bicycle = new Bicycle(20, 10, 1);
        MountainBike mountainBike = new MountainBike(20, 10, 5, "Dual");
        RoadBike roadBike = new RoadBike(40, 20, 8, 23);

        BicycleHolder[] bicycleHolders = new BicycleHolder[3];
        bicycleHolders[0] = new BicycleHolder(bicycle);
        bicycleHolders[1] = new MountainBikeHolder(mountainBike);
        bicycleHolders[2] = new RoadBikeHolder(roadBike);

        for (BicycleHolder bicycleHolder : bicycleHolders) {
            bicycleHolder.getBike().printDescription();
        }

        /*
        Console output:
        Bike is in gear 1 with a cadence of 20 and travelling at a speed of 10.

        Bike is in gear 5 with a cadence of 20 and travelling at a speed of 10.
        The MountainBike has Dual suspension.

        Bike is in gear 8 with a cadence of 40 and travelling at a speed of 20.
        The RoadBike has 23mm tires.
        */
    }

    /**
     * Tests implementation of HoldingCar class described in Task 5
     * One change: In Task 5 the class is named Car but here we use HoldingCar
     * instead since class named Car is already implemented in this project.
     */
    public static void testHoldingCar() {

        Bicycle[] bicycles = new Bicycle[3];
        bicycles[0] = new Bicycle(20, 10, 1);
        bicycles[1] = new MountainBike(20, 10, 5, "Dual");
        bicycles[2] = new RoadBike(40, 20, 8, 23);

        System.out.println("Single dispatch:");
        HoldingCar carSingle = new HoldingCar();

        for (Bicycle bike : bicycles) {
            carSingle.accept(bike);
        }

        System.out.println(carSingle);

        System.out.println("Double dispatch:");
        HoldingCar carDouble = new HoldingCar();
        for (Bicycle bike : bicycles) {
            bike.visit(carDouble);
        }
        System.out.println(carDouble);

        /*
        Output:
        Single dispatch:
        This car has:
         -holder BicycleHolder holding Bicycle
         -holder BicycleHolder holding MountainBike
         -holder BicycleHolder holding RoadBike
        Double dispatch:
        This car has:
         -holder BicycleHolder holding Bicycle
         -holder MountainBikeHolder holding MountainBike
         -holder RoadBikeHolder holding RoadBike
        */
    }

    public static void testColors() {
        BikeColor colorOfMyBike = BikeColor.GREEN;
        System.out.println("The best color is: " + colorOfMyBike.getName());
        System.out.println("The best color is: " + colorOfMyBike.name());
        System.out.println("The best color identification number is is: " + colorOfMyBike.getIdentificationNumber());

        Bicycle myBike = new Bicycle(10, 20, 1, colorOfMyBike);
        Bicycle redBike = new Bicycle(10, 20, 1, BikeColor.RED);
        Bicycle blueBike = new Bicycle(10, 20, 1, BikeColor.BLUE);

        System.out.println("myBike is " + myBike.getColor().getName());
        System.out.println("redBike is " + redBike.getColor().getName());
        System.out.println("blueBike is " + blueBike.getColor().getName());
    }

    public static void main(String[] args) {
        // Task 1
        System.out.println("======= TASK 1 ======= ");
        testBikesDescription();
        // Task 2
        System.out.println("======= TASK 2 ======= ");
        testBikeServices();
        // Task 3
        System.out.println("======= TASK 3 ======= ");
        testDoubleDispatch();
        // Task 4
        System.out.println("======= TASK 4 ======= ");
        testHolders();
        // Task 5
        System.out.println("======= TASK 5 ======= ");
        testHoldingCar();
        // Task 6
        System.out.println("======= TASK 6 ======= ");
        testColors();
        // Task 7 (try this as optional exercise, generics will be discussed later)
    }

}
