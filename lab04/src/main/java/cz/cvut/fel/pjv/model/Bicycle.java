package cz.cvut.fel.pjv.model;

import cz.cvut.fel.pjv.services.BasicService;

public class Bicycle {

    private final Integer cadence;
    private final Integer gear;
    private final Integer speed;

    public Bicycle(Integer startCadence, Integer startSpeed, Integer startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
    }

    public void printDescription() {
        System.out.println("\nBike is in gear " + this.gear
                                   + " with a cadence of " + this.cadence +
                                   " and travelling at a speed of " + this.speed + ". ");
    }

    public void visit(BasicService basicService) {
        basicService.accept(this);
    }

}
