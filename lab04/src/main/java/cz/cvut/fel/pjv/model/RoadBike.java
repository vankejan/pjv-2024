package cz.cvut.fel.pjv.model;

import cz.cvut.fel.pjv.services.BasicService;

public class RoadBike extends Bicycle {

    private final Integer tireWidth;

    public RoadBike(Integer startCadence, Integer startSpeed, Integer startGear, Integer tireWidth) {
        super(startCadence, startSpeed, startGear);
        this.tireWidth = tireWidth;
    }

    @Override
    public void printDescription() {
        super.printDescription();
        System.out.println("The RoadBike has " + tireWidth + "mm tires.");
    }

    @Override
    public void visit(BasicService basicService) {
        basicService.accept(this);
    }
}

