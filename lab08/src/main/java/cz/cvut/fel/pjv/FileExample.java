package cz.cvut.fel.pjv;

import java.io.File;

public class FileExample {

    public void printFileInformation(String pathname) {
        // TODO: check if the file exists
        // TODO: if the file exists print information about the file to stdout
        // HINT: java.io.File should be sufficient
        System.out.println("--------");
        File file = new File(pathname);
        if (file.exists()) {
            System.out.println("Name " + file.getName());
            System.out.println("Directory? " + file.isDirectory());
            System.out.println("File? " + file.isFile());
            System.out.println("Last modified: " + file.lastModified());
        }
    }
}
