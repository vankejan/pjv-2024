package cz.cvut.fel.pjv.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.pjv.model.GameConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Jackson library based configuration handler
 * TODO: implement Jackson based serialization
 * HINT: check com.fasterxml.jackson.databind.ObjectMapper
 */
public class JsonConfigurationHandler implements ConfigurationHandler {

    private static final Logger logger = Logger.getLogger(JsonConfigurationHandler.class.getName());

    private final ObjectMapper objectMapper = new ObjectMapper();

    public JsonConfigurationHandler() {
        objectMapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true);
    }

    @Override
    public void serializeConfigurationToFile(String filename, GameConfiguration gameConfiguration) {
        // TBD
        try {
            objectMapper.writeValue(new File(filename), gameConfiguration);
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

    @Override
    public Optional<GameConfiguration> deserializeConfigurationFromFile(String filename) {
        // TBD
        // HINT: check Optional class methods ( Optional.of() )

        try {
            return Optional.of(objectMapper.readValue(new File(filename), GameConfiguration.class));
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }

        return Optional.empty();
    }

    @Override
    public Optional<GameConfiguration> deserializeConfigurationFromFile(File file) {
        // TBD
        try {
            return Optional.of(objectMapper.readValue(file, GameConfiguration.class));
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }

        return Optional.empty();
    }
}
