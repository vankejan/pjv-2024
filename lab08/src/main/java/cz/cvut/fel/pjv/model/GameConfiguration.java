package cz.cvut.fel.pjv.model;


// TODO: Make the class serializable

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serial;
import java.io.Serializable;

/**
 * Represents game configuration object
 *
 */
public class GameConfiguration implements Serializable {

    @Serial
    private static final long serialVersionUID = 3865719877029130040L;


    // TODO: What happens if we save the serialized object, change the class members and then try to load the class?

    private String gameTitle;
    private int boardSizeX;
    private int boardSizeY;

    // TODO: prevent this field from being serialized (HINT: transient)
    // TODO: prevent this field from being serialized into JSON file (HINT: @JsonIgnore or mapper.configure(MapperFeature.PROPAGATE_TRANSIENT_MARKER, true))
    private transient String gameSecret;


    public String getGameTitle() {
        return gameTitle;
    }

    public void setGameTitle(String gameTitle) {
        this.gameTitle = gameTitle;
    }

    public int getBoardSizeX() {
        return boardSizeX;
    }

    public void setBoardSizeX(int boardSizeX) {
        this.boardSizeX = boardSizeX;
    }

    public int getBoardSizeY() {
        return boardSizeY;
    }

    public void setBoardSizeY(int boardSizeY) {
        this.boardSizeY = boardSizeY;
    }

    public String getGameSecret() {
        return gameSecret;
    }

    public void setGameSecret(String gameSecret) {
        this.gameSecret = gameSecret;
    }

    @Override
    public String toString() {
        return "GameConfiguration{" +
                "gameTitle='" + gameTitle + '\'' +
                ", boardSizeX=" + boardSizeX +
                ", boardSizeY=" + boardSizeY +
                ", secret=" + gameSecret +
                '}';
    }
}
