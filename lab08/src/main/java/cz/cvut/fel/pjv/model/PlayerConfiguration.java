package cz.cvut.fel.pjv.model;

import java.io.Serial;
import java.io.Serializable;

public class PlayerConfiguration implements Serializable {

    @Serial
    private static final long serialVersionUID = -3732403982774785605L;

    private final String username;

    public PlayerConfiguration(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
