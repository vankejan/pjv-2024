package cz.cvut.fel.pjv.configuration;

import cz.cvut.fel.pjv.model.GameConfiguration;

import java.io.*;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Java serialization based configuration handler
 * TODO: implement simple to/from file serialization for GameConfiguration
 * HINT: use FileOutputStream in combination with ObjectOutputStream and
 * FileInputStream with ObjectInputStream
 */
public class SimpleConfigurationHandler implements ConfigurationHandler {

    private static final Logger logger = Logger.getLogger(SimpleConfigurationHandler.class.getName());

    @Override
    public void serializeConfigurationToFile(String filename, GameConfiguration gameConfiguration) {
        // TBD
        // QUESTION: What is the correct way to handle exceptions here?
        try (FileOutputStream fileOutputStream = new FileOutputStream(filename);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);) {
            objectOutputStream.writeObject(gameConfiguration);
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

    @Override
    public Optional<GameConfiguration> deserializeConfigurationFromFile(String filename) {
        // TBD
        try (FileInputStream fileInputStream = new FileInputStream(filename);
             ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream)){
            GameConfiguration gameConfiguration = (GameConfiguration) objectInputStream.readObject();
            return Optional.of(gameConfiguration);
        } catch (ClassNotFoundException | IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<GameConfiguration> deserializeConfigurationFromFile(File filename) {
        // TBD
        return Optional.empty();
    }
}
