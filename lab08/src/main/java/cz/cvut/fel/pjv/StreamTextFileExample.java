package cz.cvut.fel.pjv;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class StreamTextFileExample {

    private static final Logger logger = Logger.getLogger(StreamTextFileExample.class.getName());

    public void readFileInputStream(String resourceFile) {
        // TODO: use just plain InputStream class and load the file as resource stream
        // HINT: the read() method will return -1 if we reach end of the file
        // NOTICE: notice the wrong encoding, try using InputStreamReader with UTF-8 encoding instead
        InputStream inputStream = StreamTextFileExample.class.getResourceAsStream(resourceFile);

        try {
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error reading from file", e);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Error closing stream", e);
            }
        }
    }

    public void readFileTryWithResources(String resourceFile) {
        // TODO: use just plain InputStream class as in previous example
        // TODO: use the try-with-resources statement
        try (InputStream inputStream = StreamTextFileExample.class.getResourceAsStream(resourceFile);) {
            int character;
            while ((character = inputStream.read()) != -1) {
                System.out.print((char) character);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error reading from file", e);
        }
    }

    public void readFileFileReader(String resourceFile) {
        logger.log(Level.FINEST, "Reading file");
        logger.log(Level.FINE, "Reading file");
        logger.log(Level.INFO, "Reading file");
        logger.log(Level.WARNING, "Reading file");
        logger.log(Level.SEVERE, "Reading file");

        // TODO: try FileReader with BufferedReader
        // QUESTION: what is the advantage of BufferedReader?
        try (FileReader fileReader = new FileReader(resourceFile);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            System.out.println(fileReader.getEncoding());
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error reading file.", e);
        }
    }

    public void readWithBufferedReader(String resourceFile) {
        // TODO: use InputStream, InputStreamReader and BufferedReader
        // NOTICE: see how the InputStreamReader serves as bridge between byte streams and character streams
        try (InputStream inputStream = StreamTextFileExample.class.getResourceAsStream(resourceFile);
             InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

            System.out.println(inputStreamReader.getEncoding());
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }

    }

    public void readFileWithNio(String resourceFile) {
        // TODO: use java.nio.file.Files.readAllLines()
        // TODO: use java.nio.file.Path.of(URI) method to get the resources file URI
        try {
            Path path = Path.of(StreamTextFileExample.class.getResource(resourceFile).toURI());
            for (String line: Files.readAllLines(path)) {
                System.out.println(line);
            }
        } catch (URISyntaxException | IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }

    public void readNonResourceFileWithNio(String filepath) {
        // TODO: use java.nio.file.Files.readAllLines()
        // TODO: use java.nio.file.Path.of(String) to get the path to the file
        try {
            Path path = Path.of(filepath);
            for (String line: Files.readAllLines(path)) {
                System.out.println(line);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }


}
