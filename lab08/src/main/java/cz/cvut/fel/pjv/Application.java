package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.configuration.ConfigurationHandler;
import cz.cvut.fel.pjv.configuration.JsonConfigurationHandler;
import cz.cvut.fel.pjv.configuration.SimpleConfigurationHandler;
import cz.cvut.fel.pjv.model.GameConfiguration;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.logging.Logger;

public class Application {

    private static final Logger logger = Logger.getLogger(Application.class.getName());

    private static final String EXAMPLE_FILE_PATH_RELATIVE = "src/main/resources/example.txt";
    private static final String EXAMPLE_FILE_PATH_RESOURCE = "/example.txt";

    // simple serialization paths
    private static final String ABSOLUTE_ROOT_SER = "/configuration.ser";
    private static final String RELATIVE_ROOT_SER = "configuration.ser";

    // json paths
    private static final String RELATIVE_ROOT_JSON = "configuration.json";

    // input json paths
    private static final String ABSOLUTE_ROOT_JSON_INPUT = "/configuration.json";
    private static final String RELATIVE_ROOT_JSON_INPUT = "configuration.json";

    public static void main(String[] args) {
        FileExample fileExample = new FileExample();
        fileExample.printFileInformation("C:/Users/vankejan/Desktop/PJV_T/pjv_2022/lab08/src/main/resources/example.txt");
        //fileExample.printFileInformation("prezentace.pdf");

        StreamTextFileExample streamTextFileExample = new StreamTextFileExample();
        System.out.println("---Input stream---");
        streamTextFileExample.readFileInputStream(EXAMPLE_FILE_PATH_RESOURCE);
        System.out.println();

        System.out.println("---Try with resources---");
        streamTextFileExample.readFileTryWithResources(EXAMPLE_FILE_PATH_RESOURCE);
        System.out.println();

        System.out.println("---File reader---");
        streamTextFileExample.readFileFileReader(EXAMPLE_FILE_PATH_RELATIVE);
        System.out.println();
//
        System.out.println("--- Buffered Reader---");
        streamTextFileExample.readWithBufferedReader(EXAMPLE_FILE_PATH_RESOURCE);
        System.out.println();

        System.out.println("---Nio---");
        streamTextFileExample.readFileWithNio(EXAMPLE_FILE_PATH_RESOURCE);
        System.out.println();


        System.out.println("---Nio non-resource---");
        streamTextFileExample.readNonResourceFileWithNio("/Users/vankejan/Desktop/PJV_T/pjv_2022/lab08/src/main/resources/example.txt");
        System.out.println();

        // OBJECT SERIALIZATION

        GameConfiguration gameConfiguration = new GameConfiguration();
        gameConfiguration.setGameTitle("Game of the year!");
        gameConfiguration.setBoardSizeX(20);
        gameConfiguration.setBoardSizeY(30);
        gameConfiguration.setGameSecret("Very secret message!!!");

        //javaSerializationExample(gameConfiguration);
        jsonSerializationExample(gameConfiguration);
    }

    private static void javaSerializationExample(GameConfiguration gameConfiguration) {
        System.out.println("-----JAVA SERIALIZATION-----");
        // try ObjectOutputStream serialize -> deserialize
        String configurationFileSerialization = RELATIVE_ROOT_SER;
        ConfigurationHandler simpleConfigurationHandler = new SimpleConfigurationHandler();
        simpleConfigurationHandler.serializeConfigurationToFile(configurationFileSerialization, gameConfiguration);
        GameConfiguration loadedConfiguration = simpleConfigurationHandler.deserializeConfigurationFromFile(configurationFileSerialization).orElseThrow();

    }

    private static void jsonSerializationExample(GameConfiguration gameConfiguration) {
        System.out.println("-----JACKSON-----");
//        // try Jackson serialize -> deserialize
        String configurationFileJson = RELATIVE_ROOT_JSON;
        ConfigurationHandler jsonConfigurationHandler = new JsonConfigurationHandler();
        jsonConfigurationHandler.serializeConfigurationToFile(configurationFileJson, gameConfiguration);
//
//
        GameConfiguration loadedConfigurationFromJson = jsonConfigurationHandler.deserializeConfigurationFromFile(configurationFileJson).orElseThrow();
        System.out.println(loadedConfigurationFromJson);
////
        // try Jackson deserialize
//        URL configurationUrl = Application.class.getResource(RELATIVE_ROOT_JSON_INPUT);
//        if (configurationUrl == null) {
//            logger.severe("Error loading configuration file.");
//        } else {
//            File jsonConfiguration = new File(configurationUrl.getFile());
//            GameConfiguration loadedConfigurationFromJsonResources = jsonConfigurationHandler.deserializeConfigurationFromFile(jsonConfiguration).orElseThrow();
//            System.out.println(loadedConfigurationFromJsonResources);
//        }
    }

}
