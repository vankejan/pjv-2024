package cz.cvut.fel.pjv.configuration;

import cz.cvut.fel.pjv.model.GameConfiguration;

import java.io.File;
import java.util.Optional;

/**
 * Interface for configuration handlers <p>
 * used for {@link GameConfiguration} class (de)serialization
 *
 *
 * @see GameConfiguration
 * @see JsonConfigurationHandler
 * @see SimpleConfigurationHandler
 */
public interface ConfigurationHandler {

    /**
     * Serialize configuration into file with given file name
     * <p>
     * Serialize the following members:
     * <p><ul>
     * <li> gameTitle
     * <li> boardSizeX
     * <li> boardSizeY
     * </ul><p>
     * Do <b>not</b> serialize:
     * <p><ul>
     * <li> gameSecret
     * </ul><p>
     *
     * @param filename Name of the file to be saved, not null
     * @param gameConfiguration Object to be serialized into file
     */
    void serializeConfigurationToFile(String filename, GameConfiguration gameConfiguration);

    /**
     * Deserialize game configuration from given file
     * <p>
     * Use full path to the file as {@code src/main/resources/example.ser}
     *
     * @param filename name of the file containing serialized object, not null
     * @return deserialized game configuration
     * @throws IllegalArgumentException thrown when ...
     */
    Optional<GameConfiguration> deserializeConfigurationFromFile(String filename) throws IllegalArgumentException;

    /**
     * Deserialize game configuration from given file
     *
     * @param file file containing serialized object, not null
     * @return deserialized game configuration
     */
    Optional<GameConfiguration> deserializeConfigurationFromFile(File file);
}
