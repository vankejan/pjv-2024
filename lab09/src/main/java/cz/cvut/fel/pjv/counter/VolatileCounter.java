package cz.cvut.fel.pjv.counter;

public class VolatileCounter implements Counter{

    private int counter = 0;

    @Override
    public void increment() {
        counter++;
    }

    @Override
    public int decrement() {
        return counter--;
    }

    @Override
    public int getValue() {
        return counter;
    }
}
