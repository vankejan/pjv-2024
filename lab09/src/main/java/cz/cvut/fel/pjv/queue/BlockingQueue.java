package cz.cvut.fel.pjv.queue;

/**
 * Thread safe implementation of a queue
 * @param <T>
 */
public interface BlockingQueue<T> {

    void put(T element) throws InterruptedException;

    T take() throws InterruptedException;
}
