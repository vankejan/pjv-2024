package cz.cvut.fel.pjv.counter;

public class SynchronizedCounter implements Counter{


    @Override
    public void increment() {

    }

    @Override
    public int decrement() {
        return 0;
    }

    @Override
    public int getValue() {
        return 0;
    }
}
