package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.counter.AtomicCounter;
import cz.cvut.fel.pjv.counter.Counter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

public class ApplicationConcurrent {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        executorServiceExample();
        //scheduledExecutorServiceExample();
        //callableExample();
    }


    private static void executorServiceExample() throws InterruptedException {
        Counter counter = new AtomicCounter();


        System.out.println(counter.getValue());
    }

    private static void scheduledExecutorServiceExample() {

    }

    private static void callableExample() throws ExecutionException, InterruptedException {

    }
}
