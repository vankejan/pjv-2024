package cz.cvut.fel.pjv.counter;

public interface Counter {

    void increment();

    int decrement();

    int getValue();
}
