package cz.cvut.fel.pjv.counter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounter implements Counter {


    @Override
    public void increment() {

    }

    @Override
    public int decrement() {
        return 0;
    }

    @Override
    public int getValue() {
        return 0;
    }
}
