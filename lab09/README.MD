[Prezentace](https://docs.google.com/presentation/d/17HfcDVfHyqf-wMeE2lNAl7YmmUdmnMOUwBbdqBayQ-A/edit?usp=share_link)

[Prezentace PDF](prezentace.pdf)

Obsah devátého cvičení:

- Vícevláknové aplikace
  - Thread, Runnable, Callable
  - ExecutorService
  - java.time API 