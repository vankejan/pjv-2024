package cz.cvut.fel.pjv.lab03.data;

import java.util.Objects;

public class TimeSpan {

    private int hour = 0;
    private int minute = 0;
    private int second = 0;

    public TimeSpan() {
    }

    public TimeSpan(TimeSpan timeSpan) {
        this.hour = timeSpan.getHour();
        this.minute = timeSpan.getMinute();
        this.second = timeSpan.getSecond();
    }

    public TimeSpan(int second) {
        this(second / 60, second % 60);
    }

    public TimeSpan(int minute, int second) {
        this(minute / 60, minute % 60, second);
    }

    public TimeSpan(int hour, int minute, int second) {
        this.setTime(hour, minute, second);
    }

    public void setTime(int hour, int minute, int second) {
        if (second >= 60 || second < 0) {
            this.second = 0;
        } else {
            this.second = second;
        }
        if (minute >= 60 || minute < 0) {
            this.minute = 0;
        } else {
            this.minute = minute;
        }

        this.hour = Math.max(hour, 0);
    }

    public TimeSpan add(int second) {
        int addMinute = 0;
        int addHour = 0;
        addMinute = (this.second + second) / 60;
        addHour = addMinute / 60;
        addMinute = addMinute % 60;
        this.second = (this.second + second) % 60;
        this.minute += addMinute;
        this.hour += addHour;
        return this;
    }

    public int getTotalSeconds() {
        return hour * 3600 + minute * 60 + second;
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public int getSecond() {
        return second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeSpan timeSpan = (TimeSpan) o;
        return getTotalSeconds() == timeSpan.getTotalSeconds();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getTotalSeconds());
    }

    @Override
    public String toString() {
        return "TimeSpan{" +
                "hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                '}';
    }
}
