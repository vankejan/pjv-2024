package cz.cvut.fel.pjv.lab03.model;

import java.util.Objects;

public class Engine {

    private final String type;

    public Engine(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Engine engine = (Engine) o;
        return Objects.equals(getType(), engine.getType());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType());
    }
}
