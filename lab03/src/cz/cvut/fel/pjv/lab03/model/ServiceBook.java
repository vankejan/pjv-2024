package cz.cvut.fel.pjv.lab03.model;

import java.util.Arrays;

public class ServiceBook {

    private static final int MAX_LENGTH = 10; // Constant - maximum number of records
    private final Car car;
    private final String[] records = new String[MAX_LENGTH];
    private int index = 0;

    public ServiceBook(Car car) {
        this.car = car;
        this.car.setServiceBook(this);
    }

    public void addRecord(String record) {
        if (index < MAX_LENGTH) {
            records[index] = record;
            index++;
        } else {
            System.out.println("Nastala chyba, servisní knížka je plná.");
        }
    }

    @Override
    public String toString() {
        /*

        // Try this using string builder and null checks to prevent the [null, null, null, null, null, null, null, null] output

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < MAX_LENGTH; i++) {
            if (records[i] != null) {
                stringBuilder.append(records[i]).append("\n");
            }
        }
        return stringBuilder.toString();
         */

        return String.format("%s %n Service records: %s", car, Arrays.toString(records));
    }
}
