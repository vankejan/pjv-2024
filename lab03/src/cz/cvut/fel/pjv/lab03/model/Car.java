package cz.cvut.fel.pjv.lab03.model;

import java.util.Objects;
import java.util.UUID;

public class Car {

    private static int numberOfExistingCars = 0; // Why this has to be static?
    private final String manufacturer;
    private final String modelName;
    private final int year;
    private final UUID vinCode;
    private Engine engineType;
    private ServiceBook serviceBook;

    public Car(String manufacturer, String modelName, Integer year) {
        this.manufacturer = manufacturer;
        this.modelName = modelName;
        this.year = year;
        this.vinCode = UUID.randomUUID();

        incrementNumberOfCars();
    }

    public Car(String manufacturer, String modelName, Integer year, String engineType) { // second constructor
        this(manufacturer, modelName, year); // call first constructor
        this.engineType = new Engine(engineType);
    }

    private void incrementNumberOfCars() { // Try explaining why this can be NON-static (instance method)
        numberOfExistingCars++;
    }

    public static int getNumberOfExistingCars() { // Try explaining why this can be static (class method)
        return numberOfExistingCars;
    }

    public void setServiceBook(ServiceBook serviceBook) {
        this.serviceBook = serviceBook;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public int getYear() {
        return year;
    }

    public String getModelName() {
        return modelName;
    }

    public UUID getVinCode() {
        return vinCode;
    }

    public ServiceBook getServiceBook() {
        return this.serviceBook;
    }

    @Override
    public String toString() {
        return String.format("%s %s year %d VIN: %s", getManufacturer(), getModelName(), getYear(), getVinCode().toString());
    }

    // Why do I need to override both the equals(..) and the hashCode()?
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Car car = (Car) o;
        return Objects.equals(getVinCode(), car.getVinCode());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getVinCode()); // this.getVinCode().hashCode(); is not NULL SAFE! will trow Nullpointer of the vinCode is null.
    }
}
