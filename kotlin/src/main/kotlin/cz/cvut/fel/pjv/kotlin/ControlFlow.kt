package cz.cvut.fel.pjv.kotlin


fun main() {
    // WHEN
    cases("Hello")
    cases(1)
    cases(0L)
    cases(123)
    cases("hey")

    // Loop
    val values = listOf("abcd", 123, "hey", "world")

    for (value in values) {
        println(value)
    }

    // fori
    for (i in 0..3) {
        println(i)
    }

    for(i in 2..8 step 2) {
        println(i)
    }

    // equality checks
    val authors = setOf("Shakespeare", "Hemingway", "Twain")
    val writers = setOf("Twain", "Shakespeare", "Hemingway")

    println(authors == writers)
    println(authors === writers)
}


fun cases(obj: Any) {
    when (obj) {                                     // 1
        1 -> println("One")                          // 2
        "Hello" -> println("Greeting")               // 3
        is Long -> println("Long")                   // 4
        !is String -> println("Not a string")        // 5
        else -> println("Unknown")                   // 6
    }
}