package cz.cvut.fel.pjv.kotlin

import kotlin.math.min

fun main(args : Array<String>) {
    val number = 123
    println("Hey there! $number")
    println("Hey there! ${number + number}")
    println("Multiply! ${sum(number, number)}")
    println("Multiply! ${multiply(number.toDouble(), number.toDouble())}")
    printWithPrefix("Hey")
    printWithPrefix("Hey", "There")
    printWithPrefix(prefix = "hey", text = "there")

    fun String.withPrefix(prefix: String) : String {return prefix + this}

    println("hey".withPrefix("There"))

    printAll("Hey", "There", "FELas")

    // high-order functions
    println(calculate(123, 123, ::sum))
    println(calculate(12, 444, ::min))

    val func = operation()
    println(func(4))
}

fun sum(x: Int, y: Int) : Int = x + y

fun multiply(x : Double, y: Double) : Double {
    return x * y
}

fun printWithPrefix(text: String, prefix: String = "Default: ") {
    println(prefix + text)
}

fun printAll(vararg messages: String) {
    for (m in messages) println(m)
}

// high order functions
fun calculate(x: Int, y: Int, operation: (Int, Int) -> Int): Int {
    return operation(x, y)
}

fun operation(): (Int) -> Int {
    return ::square
}

fun square(x: Int) = x * x