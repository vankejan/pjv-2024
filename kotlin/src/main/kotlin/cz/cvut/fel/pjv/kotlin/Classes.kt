package cz.cvut.fel.pjv.kotlin

interface ContactManager {
    fun create() : Contact?
    fun save(contact : Contact)
}

class ClientContactManager : ContactManager {
    override fun create(): Contact = ClientContact("From", "Interface", ContactAddress("Zahradní 123", "Hradec Králové"), "payment")
    override fun save(contact: Contact) {
        println("Saving contact : ${contact.name} ${contact.surname}")
    }
}


// Try changing Contact to abstract class
open class Contact(var name : String, val surname : String, val address: ContactAddress) {
    override fun toString(): String = "Contact(name='$name', surname='$surname', address=$address)"
}

data class ContactAddress(val street: String, val town : String) {
    override fun toString(): String = "Address(street=$street, town=$town)"
}

class ClientContact(name: String, surname: String, address: ContactAddress, val paymentInfo: String) : Contact(name, surname, address) {
    companion object ClientContactFactory {
        fun build() : ClientContact {
            return ClientContact("Karel", "Novák", ContactAddress("Zahradní 123", "Hradec Králové"), "payment")
        }
    }
}

object Constants {
    const val HELLO_STRING = "hello"
}

fun main(vararg args: String) {
    var contact = ClientContact("Karel", "Novák", ContactAddress("Zahradní 123", "Hradec Králové"), "payment")

    // contact = null // FAILS

    contact.name = "Josef"
    printAll(contact.toString())

    // Type inference applies, : ClientContactManager is redundant here
    val clientContactManager : ClientContactManager = ClientContactManager()

    // wrap the call with let (scope function) to be null safe
    var client : Contact? = clientContactManager.create()
    println(client?.let { clientContactManager.save(it) })


    println(Constants.HELLO_STRING)

    // companion object
    println(ClientContact.ClientContactFactory.build())

    // deconstruct classes
    val (street, town) = contact.address
    println(street)
    println(town)

    // Call Java from Kotlin
    val app : Application = Application()
    app.printInfo()
}