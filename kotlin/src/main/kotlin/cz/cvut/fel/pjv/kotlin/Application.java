package cz.cvut.fel.pjv.kotlin;

public class Application {
    public void printInfo() {
        System.out.println("Hey there folks!");
    }

    public static void main(String[] args) {
        // Call Kotlin from Java
        Contact contact = new Contact("Karel", "Novák", new ContactAddress("Zahradní", "Praha"));
        System.out.println(contact.getAddress());
    }
}
