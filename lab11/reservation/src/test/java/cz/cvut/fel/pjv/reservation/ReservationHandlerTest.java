package cz.cvut.fel.pjv.reservation;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Tag("fast")
public class ReservationHandlerTest {

    private ReservationHandler reservationHandler;

    @BeforeEach
    void setUp() {
        reservationHandler = new ReservationHandler();
    }

    @Test
    @DisplayName("Age is not between 1 and 150")
    void createReservationPriceForPassenger_ageInInvalidInterval() {
        // arrange
        Passenger passenger = new Passenger(0);
        final int price = 1000;
        // act & assert
        assertThrows(
                IllegalArgumentException.class,
                () -> reservationHandler.createReservationPriceForPassenger(passenger, price),
                "Expected reservation handler to throw exception.");
    }

}