package cz.cvut.fel.pjv.reservation;

import cz.cvut.fel.pjv.calculator.Calculator;

public class ReservationHandler {

    private static final Double CHILD_DISCOUNT = 1.0;
    private static final Double YOUNG_DISCOUNT = 0.5;
    private static final Double ELDER_DISCOUNT = 0.5;
    private static final Double STUDENT_DISCOUNT = 0.25;

    /**
     * Create reservation for passenger, including price and discount.
     * <p>
     * For passengers aged:
     * - between (0, 6> the discount is 100%
     * - between (6, 15> the discount is 50%
     * - between (15, 26> the discount is 25%
     * - between (26, 65> there is no discount
     * - between (65 - 149> there is 50% discount
     *
     * @param passenger must not be null
     * @param price     base price, must not be null
     *
     * @return reservation with price and discount
     */
    public Reservation createReservationPriceForPassenger(Passenger passenger, Integer price) {
        if (passenger.age() <= 0 || passenger.age() >= 150) {
            throw new IllegalArgumentException("Age must be withing the following interval: 0 < age < 150");
        }

        Calculator calculator = new Calculator();
        if (passenger.age() <= 6) {
            return new Reservation(price, calculator.multiply(CHILD_DISCOUNT, 100), (price - price * CHILD_DISCOUNT));
        } else if (passenger.age() <= 15) {
            return new Reservation(price, calculator.multiply(YOUNG_DISCOUNT, 100), (price - price * YOUNG_DISCOUNT));
        } else if (passenger.age() <= 26) {
            return new Reservation(price, calculator.multiply(STUDENT_DISCOUNT, 100), (price - price * STUDENT_DISCOUNT));
        } else if (passenger.age() <= 65) {
            return new Reservation(price, 0, price);
        } else {
            return new Reservation(price, calculator.multiply(ELDER_DISCOUNT, 100), (price - price * ELDER_DISCOUNT));
        }
    }

}
