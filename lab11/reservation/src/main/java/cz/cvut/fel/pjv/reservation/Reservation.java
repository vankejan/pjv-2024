package cz.cvut.fel.pjv.reservation;

public record Reservation(double price, double discountPercent, double finalPrice) {

}
