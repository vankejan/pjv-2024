[Prezentace](https://docs.google.com/presentation/d/1co71LdpofQBYWi-w7u6_Rs2SGJciCb6YwI_PGVnDz_w/edit?usp=share_link)

[Prezentace PDF](prezentace.pdf)

Obsah jedenáctého cvičení:

- Testování
  - Unit testy (Junit 5)
  - Mockování (Mockito)

- Maven
  - Dependencies
  - Plugins