package cz.cvut.fel.pjv.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("slow")
@ExtendWith(MockitoExtension.class)
public class CalculatorManagerTest {

    @Spy
    private Calculator calculator;
    private CalculatorManager calculatorManager;

    @BeforeEach
    void setUp() {
        calculatorManager = new CalculatorManager(calculator);
    }

    @Test
    void name() {
        // arrange
        Mockito.when(calculator.add(Mockito.anyInt(), Mockito.anyInt())).thenCallRealMethod();
        int expectedResult = 3;
        // act
        int actualResult = calculatorManager.makeSum(1, 1, 1);

        // assert
        assertEquals(expectedResult, actualResult);
        Mockito.verify(calculator, Mockito.times(2)).add(Mockito.anyInt(),Mockito.anyInt());
    }

}