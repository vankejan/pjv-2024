package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.calculator.CalculatorManagerTest;
import cz.cvut.fel.pjv.calculator.CalculatorTest;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({CalculatorTest.class, CalculatorManagerTest.class})
public class AllUnitTests {

}
