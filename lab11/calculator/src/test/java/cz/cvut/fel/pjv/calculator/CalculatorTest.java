package cz.cvut.fel.pjv.calculator;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@Tag("fast")
public class CalculatorTest {

    private Calculator calculator;

    @BeforeAll
    static void beforeAll() {
        System.out.println("BEFORE ALL");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("AFTER ALL");
    }

    @BeforeEach
    void setUp() {
        System.out.println("BEFORE EACH");
        calculator = new Calculator();
    }

    @AfterEach
    void tearDown() {
        System.out.println("AFTER EACH");
    }

    @Test
    @DisplayName("1 / 0 throws exception")
    void divide_divisionByZeroThrowsException() {
        IllegalArgumentException thrown = assertThrows(
                IllegalArgumentException.class,
                () -> calculator.divide(10, 0),
                "Expected calculator division by zero to throw exception.");

        assertTrue(thrown.getMessage().contains("Division by zero"));
    }

    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvSource({
            "0,    1,   1",
            "1,    2,   3",
            "49,  51, 100",
            "1,  100, 101"
    })
    void add(int first, int second, int expectedResult) {
        assertEquals(expectedResult, calculator.add(first, second),
                     () -> first + " + " + second + " should equal " + expectedResult);
    }


    @ParameterizedTest(name = "{0} + {1} = {2}")
    @CsvFileSource(resources = "/add.csv", numLinesToSkip = 1)
    void add_fileSource(int first, int second, int expectedResult) {
        assertEquals(expectedResult, calculator.add(first, second),
                     () -> first + " + " + second + " should equal " + expectedResult);
    }

}