package cz.cvut.fel.pjv.calculator;

public class Calculator {

    public int add(Integer a, Integer b) {
        return a + b;
    }

    public int divide(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Division by zero");
        }
        return a / b;
    }

    public double multiply(double a, int b) {
        return a * b;
    }

}
