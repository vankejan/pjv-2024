package cz.cvut.fel.pjv.calculator;

public class CalculatorManager {

    private final Calculator calculator;

    public CalculatorManager(Calculator calculator) {
        this.calculator = calculator;
    }

    public int makeSum(int x, int y, int z) {
        return calculator.add(calculator.add(x, y), z);
    }

}
