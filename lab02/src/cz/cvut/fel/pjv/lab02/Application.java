package cz.cvut.fel.pjv.lab02;

import java.util.Scanner;
import java.util.random.RandomGenerator;

public class Application {

    public static void main(String[] args) {
        // Task 1
        int firstNumber = 10;
        int secondNumber = 55;
        printNumberRelations(firstNumber, secondNumber);

        // Task 2a
        System.out.println(circleCircumference(12));

        // Task 2b
        System.out.println(circleArea(12));

        // Task 3
        for (int i = 1; i < 8; i++) {
            System.out.println(intToWeek(i));
        }

        // Task 4
        printStats();

        // Task 6
        int[] numbers = {1, 2, 3, 4};
        printArray(numbers);

        // Task 7
        int[] fillThisArray = new int[4];
        System.out.println("Before fillArray: ");
        printArray(fillThisArray);
        fillArray(fillThisArray);
        System.out.println("After fillArray: ");
        printArray(fillThisArray);

        // Task 8
        int[] fillThisArrayRandom = new int[20];
        fillArrayRandom(fillThisArrayRandom, 4, 7);
        System.out.println("Print randomly filled array:");
        printArray(fillThisArrayRandom);

        // Task 11
        int[][] twoDimArray = {{1, 2, 3}, {4}, {5, 6}};
        System.out.println("Print 2D array");
        print2DArray(twoDimArray);


        // Task 10
        int columns = 4;
        int rows = 5;
        int randomNumberStart = 1;
        int randomNumberEnd = 7;
        int[][] randomMatrix = create2DArrayRandom(columns, rows, randomNumberStart, randomNumberEnd);
        System.out.println("Random matrix");
        print2DArray(randomMatrix);

        // bonus
        bonus();

    }

    // Task 1
    public static void printNumberRelations(int x, int y) {
        int sum = x + y;
        System.out.println("Součet čísel X:" + x + " a Y:" + y + " je roven = " + sum);
        // System.out.format("Součet čísel X:%d a Y:%d je roven = %d%n", x, y, sum); // %n for newline, %d for decimal

        // TODO: implement rest of the operations (-, /, *)
        System.out.println(x + " - " + y + " = " + (x - y));
        System.out.println(x + " * " + y + " = " + (x * y));
        System.out.println(x + " / " + y + " = " + (x / y));

        /*
            See java tutorial about string formatting
            https://stackabuse.com/how-to-format-a-string-in-java-with-examples/
        */
    }

    // Task 2a
    public static double circleCircumference(int r) {
        /*HINT: use radius (poloměr) as input parameter */
        /*HINT: to get the PI constant use the Math library */

        return 2 * Math.PI * r;// TODO: return the circumference (obvod) of the circle

        /*
            See Math library documentation:
            https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Math.html
         */
    }

    /*TODO: Task 2b: Define function 'circleArea' that returns circle area (plochu kruhu) */
    public static double circleArea(double r) {
        return Math.PI * Math.pow(r, 2);

          /*
            See Math library documentation:
            https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/lang/Math.html
         */
    }

    // Task 3
    public static String intToWeek(int dayNumber) {
        /*
         TODO: if dayNumber is in the interval <1, 7> return name of the day (starting from Monday),
         TODO: if it is not in the interval <1, 7> return 'Invalid input'
         */
        /*HINT: use only switch statement */
        String dayName = "";
        switch (dayNumber) {
            case 1:
                dayName = "MON";
                break;
            case 2:
                dayName = "TUE";
                break;
            case 3:
                dayName = "WED";
                break;
            case 4:
                dayName = "THU";
                break;
            case 5:
                dayName = "FRI";
                break;
            case 6:
                dayName = "SAT";
                break;
            case 7:
                dayName = "SUN";
                break;
            default:
                dayName = "no such day";
        }
        return dayName;

        /*
        In Java 17 you could write: ( see switch tutorial - https://docs.oracle.com/javase/tutorial/java/nutsandbolts/switch.html)
        return switch (dayNumber) {
            case 1 -> "MON";
            case 2 -> "TUE";
            case 3 -> "WED";
            case 4 -> "THU";
            case 5 -> "FRI";
            case 6 -> "SAT";
            case 7 -> "SUN";
            default -> "no such day";
        };
        */
    }

    // Task 4
    public static void printStats() {
        /*HINT
                You do not need an array.
                Use java.util.Scanner
                Don't forger to .close() the scanner instance!
        */

        // TODO: Read integers from user, stop reading if the user enters 0
        // TODO: A - print average out of user entered sequence of numbers
        // TODO: B - print second highest number in the sequence
        // TODO: C - determine if the sequence is monotonically increasing, non-decreasing, monotonically decreasing, non-increasing or constant
        int count = 0, sum = 0;
        int highest = Integer.MIN_VALUE, secondHighest = Integer.MIN_VALUE;
        boolean ascend = false,
                descend = false,
                constant = false;

        Scanner scanner = new Scanner(System.in);
        System.out.print("gimme integer:");
        int number = 0, prevNumber = 0;
        while ((number = scanner.nextInt()) != 0) {
            if(count > 0) {
                double diff = number - prevNumber;
                if(diff > 0) {
                    ascend = true;
                } else if(diff < 0) {
                    descend = true;
                } else {
                    constant = true;
                }
            }

            if (number > highest) {
                secondHighest = highest; // shift highest to second highest
                highest = number;
            } else if (number > secondHighest) {
                secondHighest = number;
            }
            sum += number; // shorthand for sum = sum + number;
            count++; // shorthand for count = count + 1;
            prevNumber = number;
            System.out.print("gimme integer:");
        }
        scanner.close(); // IMPORTANT, do not forget to close the scanner!

        if (count == 0) {
            System.out.println("Unable to find average");
        } else {
            System.out.format("SUM= %d COUNT= %d AVG= %d %n", sum, count, sum / count); // %d for decimal, %n for newline
//            System.out.println("Sum= " + sum + "COUNT= " + count + " AVG= " + sum / count); // println version
            System.out.println("Highest= " + highest);
            System.out.println("Second Highest= " + secondHighest);

            if (ascend && !constant && !descend) {
                System.out.println("monotonically increasing");
            }
            if (ascend && constant && !descend) {
                System.out.println("non-decreasing");
            }
            if (!ascend && !constant && descend) {
                System.out.println("monotonically decreasing");
            }
            if (!ascend && constant && descend) {
                System.out.println("non-increasing");
            }
            if (!ascend && constant && !descend) {
                System.out.println("constant");
            }
        }
    }

    // Task 5 - Try it as optional homework and submit via merge requests.

    // Task 6
    public static void printArray(int[] array) {
        for(int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ", ");
        }
        // Try using enhanced for
//        for (int element : array) {
//            System.out.print(element + ", ");
//        }
        System.out.println();
    }

    // Task 7
    public static void fillArray(int[] array) {
        /*TODO: fill the array with numbers starting from 1 to array length
            Example:
            int[] array = new int[4];
            array[0] = 1;
            array[1] = 2;
            array[2] = 3;
            array[3] = 4;
         */
        for(int i = 0; i < array.length; i++) {
            array[i] = i;
        }
    }

    // Task 8
    public static void fillArrayRandom(int[] array, int min, int max) {
        /*HINT

                In java 17 use the new java.util.random.RandomGenerator API

                Like this (to generate random number between 0 and 100):
                  RandomGenerator generator = RandomGenerator.getDefault();
                  generator.nextInt(0, 100);

                In java 7 to 16 use:
                int randomNum = ThreadLocalRandom.current().nextInt(0, 10);

        */
        RandomGenerator generator = RandomGenerator.getDefault();
        for(int i = 0; i < array.length; i++) {
            array[i] = generator.nextInt(min, max);
        }
    }

    // Task 9 - Try it as optional homework and submit via merge requests.

    // Task 10
    public static int[][] create2DArrayRandom(int columns, int rows, int randomMin, int randomMax) {
        int[][] array = new int[rows][columns];
        for(int[] row : array) {
            fillArrayRandom(row, randomMin, randomMax);
        }
        return array;
    }

    // Task 11
    public static void print2DArray(int[][] matrix) {
        /*HINT lines might have different lengths! */
        for (int[] row: matrix) {
            printArray(row);
        }
    }

    // bonus
    public static void bonus() {
        double pi = Math.PI;
        System.out.format("%.3f", pi); // 3,142
        System.out.println();


        // TODO: print PI to <length> number of places after decimal
        // for length = 12 this should print 3,141592653590
        int length = 12;
        System.out.format("%."+length+"f", pi); // 3,141592653590
    }
}
