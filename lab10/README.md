[Prezentace](https://docs.google.com/presentation/d/1558jlFw5ZD5yxN4j7Xjhl7at5GxwWj8nuDFvcCVfU3A/edit?usp=share_link)

[Prezentace PDF](prezentace.pdf)

Obsah desátého cvičení:

- Sítě
  - Socket
  - Aplikace klient/server
  - Server umožňující obsluhu více klientů

## Aplikace na cvičení

Cílem je vytvořit dvě aplikace:

- Server
  - Umožní připojení více klientů souběžně.
  - Server bude počítat kolikrát dostal dané slovo.
  - Po přijetí zprávy která začíná na `COUNT <slovo>` (včetně mezery) vrátí kolikrát dostal dané slovo.
- Klient
  - Umožní připojení na server.
  - Umožní uživateli zadávat z klávesnice příkazy.
  - Bude logovat na stdout odpovědi ze serveru.


Příklad komunikace:
```
C1> Hello world
S> Counted word: Hello
S> Counted word: world
C1> Happy world
S> Counted word: Happy
S> Counted word: world
C2> Happy world
S> Counted word: Happy
S> Counted word: world
C3> COUNT world
S> 3
C2> COUNT Happy
S> 2
C1> COUNT Hello
S> 1
C1> EXIT
```
