package cz.cvut.fel.pjv.client;

import cz.cvut.fel.pjv.handler.ServerResponseHandler;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Objects;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WordCounterClient implements AutoCloseable {

    private static final String EXIT = "EXIT";
    private final Logger logger = Logger.getLogger(WordCounterClient.class.getName());
    private final Socket clientSocket;

    public WordCounterClient(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public void startConnection() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        try (PrintWriter printWriter = new PrintWriter(clientSocket.getOutputStream(), true)) {

            executorService.submit(new ServerResponseHandler(clientSocket));

            // read user input
            Scanner scanner = new Scanner(System.in);
            String input;
            while (!Objects.equals(input = scanner.nextLine(), EXIT)) {
                printWriter.println(input);
            }

        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        } finally {
            executorService.shutdown();
        }
    }

    @Override
    public void close() throws Exception {
        clientSocket.close();
    }
}
