package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.client.WordCounterClient;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 *  TODO: enable parallel run!
 *  Right corner > Run/Debug Configurations > Edit Run Configurations > Allow Multiple Instances
 */
public class ClientApplication {

    private static final Logger logger = Logger.getLogger(ClientApplication.class.getName());

    public static void main(String[] args) {
        try (WordCounterClient client = new WordCounterClient(new Socket("localhost", 6666));) {
            client.startConnection();
        } catch (Exception e) {
            logger.log(Level.SEVERE, null, e);
        }
    }
}
