package cz.cvut.fel.pjv.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerResponseHandler implements Runnable {

    private static final Logger logger = Logger.getLogger(ServerResponseHandler.class.getName());
    private final Socket socket;

    public ServerResponseHandler(Socket socket) {
        this.socket = socket;
    }

    @Override
    public void run() {
        try (InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {
            // read response from server
            String line;
            while (!socket.isClosed() && (line = bufferedReader.readLine()) != null) {
                logger.log(Level.INFO, line);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }
}
