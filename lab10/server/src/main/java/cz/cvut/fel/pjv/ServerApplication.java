package cz.cvut.fel.pjv;

import cz.cvut.fel.pjv.server.WordCounterServer;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerApplication {

    private static final Logger logger = Logger.getLogger(ServerApplication.class.getName());

    public static void main(String[] args) {
        WordCounterServer wordCounterServer = new WordCounterServer();
        try {
            wordCounterServer.start(6666);
        } catch (IOException ex) {
            logger.log(Level.SEVERE, null, ex);
        } finally {
            try {
                wordCounterServer.stop();
            } catch (IOException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
        }
    }
}
