package cz.cvut.fel.pjv.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WordCounterServerHandler implements Runnable{

    private static final Logger logger = Logger.getLogger(WordCounterServerHandler.class.getName());
    // messages
    private static final String WELCOME_MESSAGE = "Welcome to the party!";
    private static final String BYE_MESSAGE = "May the force be with you!";
    private static final String COUNTED_COMMAND_MESSAGE = "Counted word: ";
    // input messages
    private static final String TERMINATION_INPUT = "EXIT";
    private static final String COUNT_INPUT = "COUNT ";

    private final ConcurrentMap<String, Integer> dictionary;
    private final Socket socket;

    public WordCounterServerHandler(Socket socket, ConcurrentMap<String, Integer> dictionary) {
        this.socket = socket;
        this.dictionary = dictionary;
    }


    private void countWord(String word) {
        int count = 1;
        if (dictionary.containsKey(word)) {
            count = dictionary.get(word) + 1;
        }
        dictionary.put(word, count);
    }

    @Override
    public void run() {
        try (PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
             InputStreamReader inputStreamReader = new InputStreamReader(socket.getInputStream());
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        ) {
            logger.log(Level.INFO, "Initialized connection with client.");
            printWriter.println(WELCOME_MESSAGE);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals(TERMINATION_INPUT)) {
                    logger.log(Level.INFO, "Closing client connection.");
                    break;
                } else if (line.startsWith(COUNT_INPUT)) {
                    String word = line.substring(COUNT_INPUT.length());
                    printWriter.println(dictionary.getOrDefault(word, 0));
                } else {
                    for (String word : line.split(" ")) {
                        printWriter.println(COUNTED_COMMAND_MESSAGE + word);
                        countWord(word);
                    }
                }
            }
            printWriter.println(BYE_MESSAGE);
        } catch (IOException e) {
            logger.log(Level.SEVERE, null, e);
        }
    }
}
