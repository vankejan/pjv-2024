package cz.cvut.fel.pjv.server;

import cz.cvut.fel.pjv.handler.WordCounterServerHandler;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class WordCounterServer {

    private final ConcurrentMap<String, Integer> dictionary = new ConcurrentHashMap<>();
    private ServerSocket serverSocket;

    public void start(int port) throws IOException {

        ExecutorService executorService = Executors.newCachedThreadPool();
        try {
            serverSocket = new ServerSocket(port);
            while (!serverSocket.isClosed()) {
                executorService.submit(new WordCounterServerHandler(serverSocket.accept(), dictionary));
            }
        } finally {
            executorService.shutdown();
        }
    }

    public void stop() throws IOException {
        serverSocket.close();
    }
}
