V  tomto repozitáři naleznete příklady a prezentace ze cvičení předmětu Programování v JAVA pro letní semetr 2023/2024.

Pro stažení na Váš stroj použijte příkaz:

```
git clone git@gitlab.fel.cvut.cz:vankejan/pjv-2024.git
```

### Šablona

Repozitář lze také použít jako šablonu pro příklady ze cvičení. 

Pro práci prosím využijte svoji větev s názvem ve formátu:

```
student-<username>
```

> `<username>` nahraďte Vaším FEL ČVUT loginem, tedy název větve bude například `student-vankejan`

Následující příkaz vytvoří novou větev a rovnou Vás na ni přepne:

```
git checkout -b <název větve>
```

> Například tedy `git checkout -b student-vankejan` mi lokálně vytvoří novou větev a přepne mě na ni.

